package program;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class Decompressor {

    private static final int END_OF_FILE = -1;
    private static final int BITS_IN_BYTE = 8;
    private static final int REMAINING_BITS_INFO_SIZE = 3;
    private static final int UNDEFINED = -1;
    private final FileInputStream in;
    private final FileOutputStream out;

    private final Map<Integer, Byte> dictionary;

    Decompressor(String inName, String outName) throws FileNotFoundException {
        in = new FileInputStream(inName);
        out = new FileOutputStream(outName);
        dictionary = new HashMap<>();
    }

    private static int log2Ceil(int x) {
        return (int) Math.ceil((Math.log(x) / Math.log(2)));
    }

    void decompress() throws IOException {
        final int dictionarySize = in.read();
        prepareDictionary(dictionarySize);
        final int compressedValueSize = log2Ceil(dictionarySize);

        int additionalBitsAtTheEnd = 0;

        int bitsLeftFromPreviousByte = 0;
        int bitsRemainingInByte = 0;
        int previousByte = UNDEFINED;
        boolean isFirstByte = true;

        while (true) {
            int nextByte = in.read();

            if (previousByte != UNDEFINED) {
                bitsRemainingInByte += BITS_IN_BYTE;
                previousByte = previousByte + (bitsLeftFromPreviousByte << BITS_IN_BYTE);

                if (isFirstByte) {
                    isFirstByte = false;
                    bitsRemainingInByte -= REMAINING_BITS_INFO_SIZE;
                    additionalBitsAtTheEnd = previousByte >> 5;
                    previousByte = previousByte & (int) (Math.pow(2, bitsRemainingInByte) - 1);
                }

                if (nextByte == END_OF_FILE) {
                    previousByte = previousByte >> additionalBitsAtTheEnd;
                    bitsRemainingInByte -= additionalBitsAtTheEnd;
                }

                while (bitsRemainingInByte > 0) {
                    if (bitsRemainingInByte >= compressedValueSize) {
                        int key = previousByte >> (bitsRemainingInByte - compressedValueSize);
                        out.write(dictionary.get(key));

                        bitsRemainingInByte -= compressedValueSize;
                        previousByte = previousByte & (int) (Math.pow(2, bitsRemainingInByte) - 1);
                    } else {
                        bitsLeftFromPreviousByte = previousByte;
                        break;
                    }
                }
            }

            if (nextByte == END_OF_FILE) break;
            else {
                previousByte = nextByte;
            }
        }

        in.close();
        out.close();
    }

    private void prepareDictionary(int dictionarySize) throws IOException {
        for (int i = 0; i < dictionarySize; ++i) {
            byte key = (byte) in.read();
            dictionary.put(i, key);
        }
    }
}
