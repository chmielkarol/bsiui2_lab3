package program;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj nazwę pliku wejściowego: ");
        String in = scanner.nextLine();

        System.out.print("Podaj nazwę pliku wyjściowego: ");
        String out = scanner.nextLine();

        Decompressor decompressor = new Decompressor(in, out);

        decompressor.decompress();
    }
}
